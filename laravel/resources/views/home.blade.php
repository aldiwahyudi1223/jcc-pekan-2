<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
</head>
<body>
    <h1>Jabar Coding Camp</h1>

    <h2>Sosial Media Development Santai Berkualitas</h2>
    <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
    <h2>Benefit Join JabarCodingCamp</h2>
    <ul>
        <li>Berkenalan dengan struktur MVC (Bagian Controller</li>
            <li>Mengerti Routing Laravel</li>
            <li>Mengerti Penggunaan Controller di Laravel</li>
            <li>Handle Request dari URL dan form</li>
    </ul>
    <h2>Cara Bergabung ke JabarCodingCamp</h2>
    <ol>
        <li>Mengunjungi Website Ini</li>
        <li>Mendaftar di Form Daftar <a href="/daftar">Klik Disini</a></li>
        <li>Selesai</li>
    </ol>

</body>
</html>