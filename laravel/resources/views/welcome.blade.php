@extends('layout.master')

@section('content')
<div class="col-md-12">
    <!-- Widget: user widget style 1 -->
    <div class="card card-widget widget-user shadow-lg">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header text-white"
           style="background: url('{{asset('layouts')}}/dist/img/photo1.png') center center;">
        <h3 class="widget-user-username text-right">Aldi Wahyudi</h3>
        <h5 class="widget-user-desc text-right">Laravel</h5>
      </div>
      <div class="widget-user-image" width="2000px">
        <img class="img-circle" src="{{asset('layouts')}}/dist/img/aldi.png" alt="User Avatar" width="2000px">
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-sm-4 border-right">
            <div class="description-block">
              <h5 class="description-header">3,200</h5>
              <span class="description-text">SALES</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-4 border-right">
            <div class="description-block">
              <h5 class="description-header">13,000</h5>
              <span class="description-text">FOLLOWERS</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-4">
            <div class="description-block">
              <h5 class="description-header">35</h5>
              <span class="description-text">PRODUCTS</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </div>
    <!-- /.widget-user -->
  </div>
    
@endsection