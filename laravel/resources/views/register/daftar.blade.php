<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daftar</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Form Daftar</h3>
    <form action="/kirim" method="POST">
        @csrf
        <label>First name :</label> <br />
        <input type="text" name="first" /> <br /> <br>
        <label>Last name :</label> <br />
        <input type="text" name="last" /> <br />
        <br />
        <label>Gender</label><br />
        <input type="radio" /> Male <br />
        <input type="radio" /> Female <br />
        <br />
        <label for="">Nationality</label> <br />
        <select name="negara" id="negara">
          <option value="indonesia">Indonesia</option>
          <option value="English">English</option>
          <option value="Japan">Japan</option>
        </select>
        <br />
        <br />
        <label for="">Language Spoken</label><br />
        <input type="checkbox" name="bahasa" /> Indionesia <br />
        <input type="checkbox" name="bahasa" /> English <br />
        <input type="checkbox" name="bahasa" /> Other <br />
        <br />
        <label for="">Bio</label> <br />
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <br /><br />
        <input type="submit" value="Daftar" />
    </form>
</body>
</html>