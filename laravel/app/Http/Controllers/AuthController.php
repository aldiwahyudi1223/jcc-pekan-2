<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view ('register/daftar');
    }

    public function kirim(Request $request)
    {
        // dd ($request->all());
        $first = $request['first'];
        $last = $request['last'];

        return view('welcome', compact('first','last'));
    }
}
