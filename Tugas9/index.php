<?php
// $sheep = new Animal("shaun");

// echo $sheep->name; // "shaun"
// echo $sheep->legs; // 4
// echo $sheep->cold_blooded; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$animal =new Animal("shaun");
echo "name = " . $animal->name;
echo "<br>";
echo "legs = " . $animal->legs;
echo "<br>";
echo "cold_blooded = " . $animal->cold_blooded;
echo "<br><br>";

$kodok = new Frog("buduk");
echo "name = " . $kodok->name;
echo "<br>";
echo "legs = " . $kodok->legs;
echo "<br>";
echo "cold_blooded = " . $kodok->cold_blooded;
echo "<br>";
echo "Jump =". $kodok->jump() ; // "hop hop"
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "name = " . $sungokong->name;
echo "<br>";
echo "legs = " . $sungokong->legs;
echo "<br>";
echo "cold_blooded = " . $sungokong->cold_blooded;
echo "<br>";
echo "Yell = ". $sungokong->yell(); // "Auooo"


?>